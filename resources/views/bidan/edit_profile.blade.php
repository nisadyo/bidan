@extends('layout.bidan_master')
@section('content')	
	<div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Ubah Profile</h2>
            <ol class="breadcrumb">
                <li>
                	{!! link_to_route('main_page', 'KMS Bidan') !!}
                </li>
                <li class="active">
                    <strong>Ubah Profile</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content">
    	<div class="row">
    		<?php if(isset($not_match)){ ?>
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="alert alert-danger alert-dismissable">                            
                            Password tidak sesuai
                        </div>
                    </div>
                </div>
            <?php } ?>     
    		<?php if(isset($edit)){ ?>
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="alert alert-info alert-dismissable">                            
                            Profile berhasil diubah
                        </div>
                    </div>
                </div>
            <?php } ?>  
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                	<div class="ibox-content col-md-12">
                		<div class="row col-md-6 col-md-offset-3">
		                    {!! Form::open(array('route' => 'handle_edit_profile', 'class' => 'form')) !!}
                                {{ Form::hidden('id', $bidan->id) }} 
					            <div class="form-group">
					                {!! Form::label('username') !!}
					                {!! Form::text('username', $bidan->username, array('class' => 'form-username form-control', 'readonly' => 'readonly')) !!}
					            </div>
					            <div class="form-group">
					                {!! Form::label('nama') !!}
					                {!! Form::text('nama', $bidan->nama, array('class' => 'form-username form-control', 'required' => 'required')) !!}
					            </div>
					            <div class="form-group">
					                {!! Form::label('email') !!}
					                {!! Form::email('email', $bidan->email, array('class' => 'form-email form-control', 'readonly' => 'readonly')) !!}
					            </div>
					            <div class="form-group">
					                {!! Form::label('password') !!}
					                <br>
					                {!! Form::password('password', array('class' => 'form-password form-control', 'required' => 'required')) !!}
					            </div>
					            <div class="form-group">
					                {!! Form::label('confirm_password') !!}
					                <br>
					                {!! Form::password('confirm_password', array('class' => 'form-password form-control', 'required' => 'required')) !!}
					            </div>
                                <div class="form-group" id="data_1">  
                                    {!! Form::label('birthdate') !!} 
                                    <br>
                                    <div class="input-group date col-md-5 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control input-group-addon required>" name="birthdate" value="<?= $bidan->birthdate ?>">
                                    </div> 
                                </div>
					            <div class="form-group">
					                {!! Form::label('agama') !!}
					                {!! Form::text('agama', $bidan->agama, array('class' => 'form-control', 'required' => 'required')) !!}
					            </div>
					            <div class="form-group">
					                {!! Form::label('alamat') !!}
					                {!! Form::text('alamat', $bidan->alamat, array('class' => 'form-control', 'required' => 'required')) !!}
					            </div>
					            <div class="form-group">
					                {!! Form::label('nomor_induk_bidan') !!}
					                {!! Form::text('nomor_induk_bidan', $bidan->nomor_induk_bidan, array('class' => 'form-control', 'required' => 'required')) !!}
					            </div>
					            <div class="form-group">
					                {!! Form::label('lokasi_kerja') !!}
					                {!! Form::text('lokasi_kerja', $bidan->lokasi_kerja, array('class' => 'form-control', 'required' => 'required')) !!}
					            </div>
					            {!! Form::token() !!}
					    		{!! Form::submit('Edit', array('class' => 'btn btn-outline btn-block btn-info')) !!}
					        {!! Form::close() !!}
					    </div>
                	</div> 
               	</div>
            </div>
        </div>    	
    </div>  
@stop()