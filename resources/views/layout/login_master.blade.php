<!DOCTYPE html>
<html>
    <head>
        <link rel="shortcut icon" href="{{asset('public/assets/img/kms/logo.png')}}">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>KMS Bidan</title>

        <link href="{{asset('public/assets/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('public/assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet">

        <link href="{{asset('public/assets/css/animate.css')}}" rel="stylesheet">
        <link href="{{asset('public/assets/css/style.css')}}" rel="stylesheet">
    </head>

    <body class="white-bg">
        @yield('content')
    </body>
</html>
