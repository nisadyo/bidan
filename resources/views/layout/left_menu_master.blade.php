<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> 
                    <center>
                        <span>
                            <img alt="image" class="img-circle" src="{{asset('public/assets/img/kms/logo.png')}}" />
                        </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{ \Auth::user()->nama }}</strong>
                         </span> <span class="text-muted text-xs block">Bidan<b class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li>{!! link_to_route('edit_profile', 'Profile', array(\Auth::user()->id)) !!}</li>
                            <li class="divider"></li>
                            <li>                                
                                {!! Html::decode(link_to_route('logout', '<i class="fa fa-sign-out"></i> <span class="nav-label">Log Out</span>')) !!}
                            </li>
                        </ul>
                    </center>                    
                </div>
                <div class="logo-element">
                    <img alt="image" class="img-circle" src="{{asset('public/assets/img/kms/logo.png')}}" />
                </div>
            </li>
            <li>                
                {!! Html::decode(link_to_route('main_page', '<i class="fa fa-home"></i> <span class="nav-label">Home</span>')) !!}
            </li>
            <li>
                {!! Html::decode(link_to_route('choose_orang_tua', '<i class="fa fa-child"></i> <span class="nav-label">Tumbuh Anak</span>')) !!}
            </li>
        </ul>
    </div>
</nav>