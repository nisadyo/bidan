<!-- Mainly scripts -->
<script src="{{asset('public/assets/js/jquery-2.1.1.js')}}"></script>
<script src="{{asset('public/assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('public/assets/js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
<script src="{{asset('public/assets/js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

<!-- Custom and plugin javascript -->
<script src="{{asset('public/assets/js/inspinia.js')}}"></script>
<script src="{{asset('public/assets/js/plugins/pace/pace.min.js')}}"></script>

<!-- jQuery UI -->
<script src="{{asset('public/assets/js/plugins/jquery-ui/jquery-ui.min.js')}}"></script>

<script src="{{asset('public/assets/js/plugins/dataTables/datatables.min.js')}}"></script>
<script src="{{asset('public/assets/js/plugins/jeditable/jquery.jeditable.js')}}"></script>

<script  src="{{asset('public/assets/js/plugins/datapicker/bootstrap-datepicker.js')}}"></script>

<script src="{{asset('public/assets/js/plugins/c3/c3.min.js')}}"></script>
<script src="{{asset('public/assets/js/plugins/d3/d3.min.js')}}"></script>

<!-- Page-Level Scripts -->
<script>
    $(document).ready(function(){
        $('.dataTables-example').DataTable({
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                {extend: 'copy'},
                {extend: 'csv'},
                {extend: 'excel', title: 'ExampleFile'},
                {extend: 'pdf', title: 'ExampleFile'},

                {extend: 'print',
                 customize: function (win){
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                }
                }
            ]
        });

        <?php if(isset($hasil_tb_u)){ ?>
            c3.generate({
                bindto: '#tb_u',
                data:{
                    x: 'Anak',
                    columns: [
                        ['Anak', <?php foreach ($hasil_tb_u as $h) { if($h->usia >= 0) {echo $h->usia;} ?>, <?php } ?>],
                        ['Tinggi_Anak', <?php foreach ($hasil_tb_u as $h) { if($h->usia >= 0) {echo $h->tinggi;} ?>, <?php } ?>],
                        ['Sangat_Pendek', <?php foreach ($hasil_tb_u as $h) { if($h->usia >= 0) {echo $h->sangat_pendek;} ?>, <?php } ?>],
                        ['Pendek', <?php foreach ($hasil_tb_u as $h) { if($h->usia >= 0) {echo $h->pendek;} ?>, <?php } ?>],                        
                        ['Normal', <?php foreach ($hasil_tb_u as $h) { if($h->usia >= 0) {echo $h->normal;} ?>, <?php } ?>],
                        ['Tinggi', <?php foreach ($hasil_tb_u as $h) { if($h->usia >= 0) {echo $h->normal + 1;} ?>, <?php } ?>]
                    ],
                    colors:{
                        Tinggi_Anak: '#1ab394'
                    }
                },
                axis: {
                    x: {
                        label: 'Umur (Bulan)'
                    },
                    y: {
                        label: 'Tinggi (cm)'
                    }
                }
            });
        <?php } ?>

        <?php if(isset($hasil_bb_u)){ ?>
            c3.generate({
                bindto: '#bb_u',
                data:{
                    x: 'Anak',
                    columns: [
                        ['Anak', <?php foreach ($hasil_bb_u as $h) { if($h->usia >= 0) {echo $h->usia;} ?>, <?php } ?>],
                        ['Berat_Anak', <?php foreach ($hasil_bb_u as $h) { if($h->usia >= 0) {echo $h->berat;} ?>, <?php } ?>],
                        ['Buruk', <?php foreach ($hasil_bb_u as $h) { if($h->usia >= 0) {echo $h->buruk;} ?>, <?php } ?>],
                        ['Kurang', <?php foreach ($hasil_bb_u as $h) { if($h->usia >= 0) {echo $h->kurang;} ?>, <?php } ?>],                        
                        ['Baik', <?php foreach ($hasil_bb_u as $h) { if($h->usia >= 0) {echo $h->baik;} ?>, <?php } ?>],
                        ['Lebih', <?php foreach ($hasil_bb_u as $h) { if($h->usia >= 0) {echo $h->baik + 1;} ?>, <?php } ?>]
                    ],
                    colors:{
                        Berat_Anak: '#1ab394'
                    }
                },
                axis: {
                    x: {
                        label: 'Umur (Bulan)'
                    },
                    y: {
                        label: 'Berat (kg)'
                    }
                }
            });
        <?php } ?>

        <?php if(isset($hasil_tb_bb)){ ?>
            c3.generate({
                bindto: '#tb_bb',
                data:{
                    x: 'Tinggi',
                    columns: [
                        ['Tinggi', <?php foreach ($hasil_tb_bb as $h) { if($h->usia >= 0) {echo $h->tinggi;} ?>, <?php } ?>],
                        ['Berat_Anak', <?php foreach ($hasil_tb_bb as $h) { if($h->usia >= 0) {echo $h->berat;} ?>, <?php } ?>],
                        ['Sangat_Kurus', <?php foreach ($hasil_tb_bb as $h) { if($h->usia >= 0) {echo $h->sangat_kurus + 0;} ?>, <?php } ?>],
                        ['Kurus', <?php foreach ($hasil_tb_bb as $h) { if($h->usia >= 0) {echo $h->kurus + 0;} ?>, <?php } ?>],                        
                        ['Normal', <?php foreach ($hasil_tb_bb as $h) { if($h->usia >= 0) {echo $h->normal + 0;} ?>, <?php } ?>],
                        ['Gemuk', <?php foreach ($hasil_tb_bb as $h) { if($h->usia >= 0) {echo $h->gemuk + 0;} ?>, <?php } ?>],
                        ['Sangat_Gemuk', <?php foreach ($hasil_tb_bb as $h) { if($h->usia >= 0) {echo $h->sangat_gemuk + 0.9;} ?>, <?php } ?>]
                    ],
                    colors:{
                        Berat_Anak: '#1ab394'
                    }
                },
                axis: {
                    x: {
                        label: 'Tinggi (cm)'
                    },
                    y: {
                        label: 'Berat (kg)'
                    }
                }
            });
        <?php } ?>

        $('.modal-button').click(function(){
            $("#id_parameter_tumbuh").val($(this).attr('data_parameter'));
            document.getElementById("detail_tumbuh").innerHTML = $(this).attr('data_detail') + ' ?';
            $("#id_tumbuh").val($(this).attr('data_id'));
            $("#id_tumbuh_2").val($(this).attr('data_id_2'));
            $("#berat_tumbuh").val($(this).attr('data_berat'));            
            $("#tinggi_tumbuh").val($(this).attr('data_tinggi'));
        });

        $('#data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true
        });

    });
</script>