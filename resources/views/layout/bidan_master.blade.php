<!DOCTYPE html>
<html>
    <head>
        <link rel="shortcut icon" href="{{asset('public/assets/img/kms/logo.png')}}">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>KMS Bidan</title>

        <link href="{{asset('public/assets/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('public/assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet">

        <link href="{{asset('public/assets/css/plugins/dataTables/datatables.min.css')}}" rel="stylesheet">

        <link href="{{asset('public/assets/css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">
        
        <link href="{{asset('public/assets/css/plugins/c3/c3.min.css')}}" rel="stylesheet">

        <link href="{{asset('public/assets/css/animate.css')}}" rel="stylesheet">
        <link href="{{asset('public/assets/css/style.css')}}" rel="stylesheet">
    </head>

    <body>
        <div id="wrapper">
            @include('layout.left_menu_master')
            <div id="page-wrapper" class="gray-bg dashbard-1">
                @include('layout.top_menu_master')
                @yield('content')       
                @include('layout.footer_master')
            </div>
        </div>        
        @include('layout.javascript_master')
    </body>
</html>
