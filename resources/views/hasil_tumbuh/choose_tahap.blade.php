@extends('layout.bidan_master')
@section('content')	
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Pilih Tahap Pertumbuhan</h2>
            <ol class="breadcrumb">
                <li>
                	{!! link_to_route('main_page', 'KMS Bidan') !!}
                </li>
                <li>
                    <a>Tumbuh Anak</a>
                </li>
                <li>
                    {!! link_to_route('choose_orang_tua', 'Pilih Orang Tua') !!}
                </li>
                <li>
                    {!! link_to_route('choose_anak', 'Pilih Anak', $parameter_tumbuh->orang_tua) !!}
                </li>
                <li class="active">
                    <strong>Pilih Tahap Pertumbuhan</strong>
                </li>
            </ol>
        </div>
    </div>
	<div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">            
            <div class="col-lg-12">
                <?php if($action == 'update'){ ?>
                    <div class="alert alert-info alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        Data pertumbuhan pada <?= $tahap ?> berhasil di ubah
                    </div>
                <?php } else if($action == 'add'){?>
                    <div class="alert alert-success alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        Data pertumbuhan pada <?= $tahap ?> berhasil di tambah
                    </div>
                <?php } else if($action == 'delete'){?>
                    <div class="alert alert-danger alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        Data pertumbuhan pada <?= $tahap ?> berhasil di hapus
                    </div>
                <?php } else if($action == 'berat'){?>
                    <div class="alert alert-warning alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        Data berat pada <?= $tahap ?> harus bertipe angka
                    </div>
                <?php } else if($action == 'tinggi'){?>
                    <div class="alert alert-warning alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        Data tinggi pada <?= $tahap ?> harus bertipe angka
                    </div>
                <?php } else if($action == 'tanggal'){?>
                    <div class="alert alert-warning alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        Data tanggal pada <?= $tahap ?> harus diisi
                    </div>
                <?php } ?>
            </div>
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="table-responsive">
                    		<table class="table table-striped table-bordered table-hover" >
                    			<thead>
				                    <tr>
                                        <th width="3%">No</th>
				                        <th width="19%">Tahap Pertumbuhan</th>
                                        <th width="10%">Berat</th>
                                        <th width="10%">Tinggi</th>
                                        <th>Bidan</th>
                                        <th width="14%">Tanggal Input</th>
                                        <th width="5%">Input</th>
                                        <th width="5%">Ubah</th>
                                        <th width="5%">Hapus</th>
				                    </tr>
			                    </thead>
                    			<tbody>
                    				<?php foreach ($parameter_tumbuh as $p) { ?>
					                    <tr>
                                            <td>{!! $p->id !!}</td>
					                        <td>{!! $p->detail_tumbuh !!}</td>
                                            <td>{!! $p->berat !!}<?= $p->berat != '-' ? ' kg' : '' ?></td>
                                            <td>{!! $p->tinggi !!}<?= $p->tinggi != '-' ? ' cm' : '' ?></td>
                                            <td>{!! $p->bidan !!}</td>
                                            <td>{!! $p->tanggal_input !!}</td>
                                            <?php if($p->berat != '-' && $p->tinggi != '-'){ ?>
					                            <td><center>
                                                    {!! Html::decode(link_to_route('choose_tahap', '<i class="fa fa-plus"></i> ', array($parameter_tumbuh->id_anak), ['class' => 'btn btn-outline btn-circle btn-success', 'disabled' => 'disabled', 'title' => 'Data telah ada'])) !!}
                                                </td>
                                                    <?php if($p->bidan_id == $parameter_tumbuh->user){ ?>
                                                        <td><center>
                                                            <button type="button" class="btn btn-outline btn-circle btn-info modal-button" data-toggle="modal" data-target="#edit_modal" data_id="{{ $p->id_hasil_tumbuh }}" data_berat="{{ $p->berat }}" data_tinggi="{{ $p->tinggi }}">
                                                                <i class="fa fa-edit"></i>
                                                            </button>
                                                            <div class="modal inmodal" id="edit_modal" tabindex="-1" role="dialog"  aria-hidden="true">
                                                                <div class="modal-dialog modal-sm">
                                                                    <div class="modal-content animated fadeIn">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                                            <h5 class="modal-title">Ubah Data Pertumbuhan</h5>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            {!! Form::open(array('route' => 'edit_tumbuh', 'class' => 'form-horizontal form-label-left', 'id' => 'demo-form2')) !!}
                                                                                {{ Form::hidden('id', '', array('id' => 'id_tumbuh')) }}    
                                                                            <div class="form-group"> 
                                                                                {!! Form::label('berat', null, array('class' => 'control-label col-sm-3 col-xs-12')) !!}
                                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                    {!! Form::text('berat', '', array('id' => 'berat_tumbuh', 'class' => 'form-control', 'required' => 'required', 'berat' => 'berat_tumbuh')) !!}
                                                                                </div> 
                                                                                {!! Form::label('kg', 'kg', array('class' => 'control-label col-sm-1 col-xs-12')) !!}
                                                                            </div>
                                                                            <div class="form-group">                     
                                                                                {!! Form::label('tinggi', null, array('class' => 'control-label col-sm-3 col-xs-12')) !!}
                                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                    {!! Form::text('tinggi', '', array('id' => 'tinggi_tumbuh', 'class' => 'form-control', 'required' => 'required')) !!}
                                                                                </div>
                                                                                {!! Form::label('cm', 'cm', array('class' => 'control-label col-sm-1 col-xs-12')) !!}
                                                                            </div>
                                                                            {!! Form::token() !!}
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-outline btn-white" data-dismiss="modal">Batal</button>
                                                                            {!! Form::submit('Ubah Data', array('class' => 'btn btn-outline btn-info')) !!}
                                                                        </div>                    
                                                                        {!! Form::close() !!}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td><center>
                                                            <button type="button" class="btn btn-outline btn-circle btn-danger modal-button" data-toggle="modal" data-target="#delete_modal" data_detail="{{ $p->detail_tumbuh }}" data_id_2="{{ $p->id_hasil_tumbuh }}">
                                                                <i class="fa fa-trash"></i>
                                                            </button>
                                                            <div class="modal inmodal" id="delete_modal" tabindex="-1" role="dialog"  aria-hidden="true">
                                                                <div class="modal-dialog modal-sm">
                                                                    <div class="modal-content animated fadeIn">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                                            <h5 class="modal-title">Hapus Data Pertumbuhan</h5>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            Anda yakin ingin menghapus data pertumbuhan <text id="detail_tumbuh" type="text"> ?
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            {!! Form::open(array('route' => 'delete_tumbuh', 'class' => 'form-horizontal form-label-left', 'id' => 'demo-form2')) !!}  
                                                                            <button type="button" class="btn btn-outline btn-white" data-dismiss="modal">Batal</button>    
                                                                                {{ Form::hidden('id', '', array('id' => 'id_tumbuh_2')) }} 
                                                                                {!! Form::submit('Hapus Data', array('class' => 'btn btn-outline btn-danger')) !!}
                                                                            {!! Form::close() !!}
                                                                        </div>               
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    <?php }else { ?>                      
                                                        <td><center>{!! Html::decode(link_to_route('choose_tahap', '<i class="fa fa-edit"></i> ', array($parameter_tumbuh->id_anak), ['class' => 'btn btn-outline btn-circle btn-info', 'disabled' => 'disabled', 'title' => 'Data di kelola oleh bidan lain'])) !!}</td>
                                                        <td><center>{!! Html::decode(link_to_route('choose_tahap', '<i class="fa fa-trash"></i> ', array($parameter_tumbuh->id_anak), ['class' => 'btn btn-outline btn-circle btn-danger', 'disabled' => 'disabled', 'title' => 'Data di kelola oleh bidan lain'])) !!}</td>
                                                    <?php  }
                                                }else { ?>
                                                <td><center>
                                                    <button type="button" class="btn btn-outline btn-circle btn-info modal-button" data-toggle="modal" data-target="#tambah_modal" data_parameter="{{ $p->id }}">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                    <div class="modal inmodal" id="tambah_modal" tabindex="-1" role="dialog"  aria-hidden="true">
                                                        <div class="modal-dialog modal-sm">
                                                            <div class="modal-content animated fadeIn">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                                    <h5 class="modal-title">Tambah Data Pertumbuhan</h5>
                                                                </div>
                                                                <div class="modal-body">
                                                                    {!! Form::open(array('route' => 'input_tumbuh', 'class' => 'form-horizontal form-label-left', 'id' => 'demo-form2')) !!}
                                                                        {{ Form::hidden('parameter__tumbuh_id', '', array('id' => 'id_parameter_tumbuh')) }}
                                                                        {{ Form::hidden('anak_id', $parameter_tumbuh->id_anak) }}
                                                                        {{ Form::hidden('bidan_id', $parameter_tumbuh->user) }}
                                                                    <div class="form-group" id="data_1">  
                                                                        {!! Form::label('tanggal', null, array('class' => 'control-label col-sm-3 col-xs-12')) !!} 
                                                                        <div class="input-group date col-md-5 col-md-offset-4 col-sm-6 col-xs-12">
                                                                            <input type="text" class="form-control input-group-addon" name="tanggal">
                                                                        </div> 
                                                                    </div>
                                                                    <div class="form-group">   
                                                                        {!! Form::label('berat', null, array('class' => 'control-label col-sm-3 col-xs-12')) !!}
                                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                                            {!! Form::text('berat', null, array('class' => 'form-control', 'required' => 'required', 'placeholder' => '00.00')) !!}
                                                                        </div> 
                                                                        {!! Form::label('kg', 'kg', array('class' => 'control-label col-sm-1 col-xs-12')) !!}
                                                                    </div>
                                                                    <div class="form-group">                     
                                                                        {!! Form::label('tinggi', null, array('class' => 'control-label col-sm-3 col-xs-12')) !!}
                                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                                            {!! Form::text('tinggi', null, array('class' => 'form-control', 'required' => 'required', 'placeholder' => '00.00')) !!}
                                                                        </div>
                                                                        {!! Form::label('cm', 'cm', array('class' => 'control-label col-sm-1 col-xs-12')) !!}
                                                                    </div>
                                                                    {!! Form::token() !!}
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-outline btn-white" data-dismiss="modal">Batal</button>
                                                                    {!! Form::submit('Tambah Data', array('class' => 'btn btn-outline btn-success')) !!}
                                                                </div>                    
                                                                {!! Form::close() !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td><center>{!! Html::decode(link_to_route('choose_tahap', '<i class="fa fa-edit"></i> ', array($parameter_tumbuh->id_anak), ['class' => 'btn btn-outline btn-circle btn-info', 'disabled' => 'disabled', 'title' => 'Data belum ada'])) !!}</td>
                                                <td><center>{!! Html::decode(link_to_route('choose_tahap', '<i class="fa fa-trash"></i> ', array($parameter_tumbuh->id_anak), ['class' => 'btn btn-outline btn-circle btn-danger', 'disabled' => 'disabled', 'title' => 'Data belum ada'])) !!}</td>
                                            <?php } ?>  
					                    </tr>
				                    <?php } ?>
			                    </tbody>
		                    </table>
                    	</div>
                    </div>
                </div>
            </div>
    	</div>
    </div>
@stop()