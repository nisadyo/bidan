@extends('layout.bidan_master')
@section('content')	
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Pilih Anak</h2>
            <ol class="breadcrumb">
                <li>
                	{!! link_to_route('main_page', 'KMS Bidan') !!}
                </li>
                <li>
                    <a>Tumbuh Anak</a>
                </li>
                <li>
                    {!! link_to_route('choose_orang_tua', 'Pilih Orang Tua') !!}
                </li>
                <li class="active">
                    <strong>Pilih Anak</strong>
                </li>
            </ol>
        </div>
    </div>
	<div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="table-responsive">
                    		<table class="table table-striped table-bordered table-hover dataTables-example" >
                    			<thead>
				                    <tr>
				                        <th>Nama Anak</th>
                                        <th width="13%">Hasil BB/U</th>
                                        <th width="13%">Hasil TB/U</th>
                                        <th width="13%">Hasil TB/BB</th>
				                        <th width="20%">Input Tumbuh Anak</th>
				                    </tr>
			                    </thead>
                    			<tbody>
                    				<?php foreach ($anak as $a) { ?>
					                    <tr>
					                        <td>{!! $a->nama !!}</td>
                                            <td>{!! Html::decode(link_to_route('view_hasil_bb_u', '<i class="fa fa-arrows-h"></i> ', array($a->id), ['class' => 'btn btn-outline btn-circle btn-primary'])) !!}</td>
                                            <td>{!! Html::decode(link_to_route('view_hasil_tb_u', '<i class="fa fa-arrow-up"></i> ', array($a->id), ['class' => 'btn btn-outline btn-circle btn-primary'])) !!}</td>
                                            <td>{!! Html::decode(link_to_route('view_hasil_tb_bb', '<i class="fa fa-line-chart"></i> ', array($a->id), ['class' => 'btn btn-outline btn-circle btn-primary'])) !!}</td>
					                        <td>{!! Html::decode(link_to_route('choose_tahap', '<i class="fa fa-plus"></i> ', array($a->id), ['class' => 'btn btn-outline btn-circle btn-success'])) !!}</td>
					                    </tr>
				                    <?php } ?>
			                    </tbody>
		                    </table>
                    	</div>
                    </div>
                </div>
            </div>
    	</div>
    </div>
@stop()