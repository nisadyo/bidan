@extends('layout.bidan_master')
@section('content') 
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Pilih Tahap Pertumbuhan</h2>
            <ol class="breadcrumb">
                <li>
                    {!! link_to_route('main_page', 'KMS Bidan') !!}
                </li>
                <li>
                    <a>Tumbuh Anak</a>
                </li>
                <li>
                    {!! link_to_route('choose_orang_tua', 'Pilih Orang Tua') !!}
                </li>
                <li>
                    {!! link_to_route('choose_anak', 'Pilih Anak', $hasil_tb_u->orangtua) !!}
                </li>
                <li class="active">
                    <strong>Grafik TB/U</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Grafik Tinggi Badan/Umur {!! $hasil_tb_u->anak !!} 0 bulan - 60 bulan</h5>
                    </div>
                    <div class="ibox-content">
                        <div>
                            <div id="tb_u"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Penjelasan Grafik Tinggi Badan/Umur {!! $hasil_tb_u->anak !!} 0 bulan - 60 bulan</h5>
                    </div>
                    <div class="ibox-content">
                        <div>
                            <?php foreach ($hasil_tb_u as $h) { if($h->usia >= 0) {?>
                                <li><ol>Pada usia <?= $h->detail_tumbuh ?>, anak terdiagnosis <?= $h->hasil ?></ol></li>
                            <?php }} ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop()