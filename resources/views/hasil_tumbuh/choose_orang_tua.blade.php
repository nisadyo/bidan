@extends('layout.bidan_master')
@section('content')	
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Pilih Orang Tua</h2>
            <ol class="breadcrumb">
                <li>
                	{!! link_to_route('main_page', 'KMS Bidan') !!}
                </li>
                <li>
                    <a>Tumbuh Anak</a>
                </li>
                <li class="active">
                    <strong>Pilih Orang Tua</strong>
                </li>
            </ol>
        </div>
    </div>
	<div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="table-responsive">
                    		<table class="table table-striped table-bordered table-hover dataTables-example" >
                    			<thead>
				                    <tr>
				                        <th>Nama Ibu</th>
				                        <th width="15%">Pilih Ibu</th>
				                    </tr>
			                    </thead>
                    			<tbody>
                    				<?php foreach ($orang_tua as $o) { ?>
					                    <tr>
					                        <td>
					                        	{!! $o->ibu_nama !!}
					                        </td>
					                        <td>
					                        	{!! Html::decode(link_to_route('choose_anak', '<i class="fa fa-hand-o-right"></i> ', array($o->id), ['class' => 'btn btn-outline btn-circle btn-primary dim'])) !!}
					                        </td>
					                    </tr>
				                    <?php } ?>
			                    </tbody>
		                    </table>
                    	</div>
                    </div>
                </div>
            </div>
    	</div>
    </div>
@stop()