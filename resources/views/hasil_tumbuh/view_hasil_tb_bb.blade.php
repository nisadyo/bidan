@extends('layout.bidan_master')
@section('content') 
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Pilih Tahap Pertumbuhan</h2>
            <ol class="breadcrumb">
                <li>
                    {!! link_to_route('main_page', 'KMS Bidan') !!}
                </li>
                <li>
                    <a>Tumbuh Anak</a>
                </li>
                <li>
                    {!! link_to_route('choose_orang_tua', 'Pilih Orang Tua') !!}
                </li>
                <li>
                    {!! link_to_route('choose_anak', 'Pilih Anak', $hasil_tb_bb->orangtua) !!}
                </li>
                <li class="active">
                    <strong>Grafik TB/BB</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Grafik Tinggi Badan/Berat Badan {!! $hasil_tb_bb->anak !!} 0 bulan - 60 bulan</h5>
                    </div>
                    <div class="ibox-content">
                        <div>
                            <div id="tb_bb"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Penjelasan Grafik Tinggi Badan/Berat Badan {!! $hasil_tb_bb->anak !!} 0 bulan - 60 bulan</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#">Config option 1</a>
                                </li>
                                <li><a href="#">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="panel-body">
                            <div class="panel-group" id="accordion">
                                <?php foreach ($hasil_tb_bb as $h) { if($h->usia >= 0) {?>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $h->id ?>">Pada usia <?= $h->detail_tumbuh ?>, anak terdiagnosis <?= $h->hasil ?></a>
                                            </h5>
                                        </div>
                                        <div id="collapse<?= $h->id ?>" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <?php foreach ($h->gizi as $gizi) { 
                                                    if($gizi->umur == $h->umur) {?>
                                                        <li><ol><?= $gizi->saran; ?></ol>
                                                        <ol><ol><?= $gizi->keterangan; ?></ol></ol></li>
                                                    <?php }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php }} ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop()