@extends('layout.login_master')
@section('content')
	<div class="loginColumns animated fadeInDown">
        <div class="row">          
            <div class="col-md-12">
                <h2 class="font-bold col-sm-offset-6">KMS Bidan</h2>
                <div class="row">
                    <div class="col-lg-7 b-r">
                        <center><img class="col-lg-10 col-md-offset-2" src="{{asset('public/assets/img/kms/button_bidan.png')}}" style="background-size: contain;"/></center>
                    </div>
                    <div class="col-lg-5">
                        <?php if(isset($error)){ ?>
                            <div class="col-lg-12">
                                <div class="ibox float-e-margins">
                                    <div class="alert alert-danger alert-dismissable">                            
                                        Username atau password salah
                                    </div>
                                </div>
                            </div>
                        <?php } else { ?>  
                            <pre style="opacity:0;">

                                a

                            </pre>
                        <?php } ?>
                        {!! Form::open(array('route' => 'handleLogin')) !!}
                            <div class="form-group">
                                {!! Form::text('username', null, array('class' => 'form-control', 'placeholder' => 'Username', 'required' => 'required')) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password', 'required' => 'required')) !!}
                            </div>
                            {!! Form::token() !!}
                            {!! Form::submit('Log in', array('class' => 'btn btn-primary block full-width m-b')) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop()