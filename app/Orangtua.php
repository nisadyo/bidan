<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Anak;

class Orangtua extends Model
{
    protected $table = 'orangtua';

    protected $fillable = [
        'nama', 'email', 'password', 'username', 'alamat', 'birthdate', 'kota', 'agama',
    ];

    protected $hidden = [
        'password', 'remember_token', 'created_at', 'updated_at', 'id', 'username',
    ];

    public function anak(){
        return $this->hasMany('App\Anak');
    }

    public function detail_anak($id){
        return $this->Has('App\Anak');
    }
}
