<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Bidan extends Authenticatable
{
    protected $table = 'bidan';

    protected $fillable = [
        'nama', 'username', 'email', 'password', 'birthdate', 'agama', 'alamat', 'nomor_induk_bidan', 'lokasi_kerja', 
    ];

    protected $hidden = [
    	'password', 'remember_token', 'created_at', 'updated_at', 'id', 'username',
    ];

    public function hasil_tumbuh(){
    	return $this->hasMany('App\Hasil_Tumbuh');
    }
}
