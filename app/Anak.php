<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Anak extends Model
{
    use SoftDeletes;

    protected $table = 'anak';

    protected $fillable = [
        'nama', 'gender', 'birthdate',
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at', 'id',
    ];

    protected $dates =[
      'deleted_at'  ,
    ];

    public function orangtua(){
        return $this->belongsTo('App\Orangtua');
    }

    public function umur(){
        $now = Carbon::now('Asia/Jakarta');
        $birthdate = new Carbon($this->birthdate);
        return($now->diffInYears($birthdate));
    }

    public function hasil_tumbuh(){
        return $this->hasMany('App\Hasil_Tumbuh');
    }
}
