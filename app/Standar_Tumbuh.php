<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Standar_Tumbuh extends Model
{
    use SoftDeletes;

    protected $table = 'standartumbuh';

    protected $fillable = [
        'umur', 'jenis_kelamin', 'tinggi_badan', 'sangat_kurus', 'kurus', 'normal', 'gemuk', 'sangat_gemuk',
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at', 'id',
    ];

    protected $dates =[
      'deleted_at',
    ];
}
