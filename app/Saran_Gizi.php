<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Saran_Gizi extends Model
{
    use SoftDeletes;

    protected $table = 'saran_gizis';

    protected $fillable = [
        'umur', 'status_gizi', 'saran', 'keterangan',
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at', 'id',
    ];

    protected $dates =[
      'deleted_at',
    ];
}
