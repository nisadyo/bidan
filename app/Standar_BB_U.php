<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Standar_BB_U extends Model
{
    use SoftDeletes;

    protected $table = 'standarbbu';

    protected $fillable = [
        'umur', 'buruk', 'kurang', 'baik',
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at', 'id',
    ];

    protected $dates =[
      'deleted_at',
    ];
}
