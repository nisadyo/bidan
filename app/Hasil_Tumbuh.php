<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hasil_Tumbuh extends Model
{
    use SoftDeletes;

    protected $table = 'hasiltumbuh';

    protected $fillable = [
        'tanggal', 'berat', 'tinggi', 'parameter__tumbuh_id', 'bidan_id', 'anak_id',
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at', 'id', 'parameter__tumbuh_id', 'bidan_id', 'anak_id',
    ];

    protected $dates =[
      'deleted_at',
    ];

    public static $validate = [
        'tanggal' => 'Required|Date',
        'berat' => 'Required|Numeric',
        'tinggi' => 'Required|Numeric'
    ];

    public function anak(){
        return $this->belongsTo('App\Anak');
    }

    public function bidan(){
        return $this->belongsTo('App\Bidan');
    }

    public function parameter_tumbuh(){
        return $this->belongsTo('App\Parameter_Tumbuh');
    }
}
