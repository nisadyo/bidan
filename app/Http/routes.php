<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::resource('posts', 'PostsController');
    
    //login
    Route::get('/', ['as' => 'login', 'uses' => 'login_controller@login']);
    Route::get('/login', ['as' => 'login', 'uses' => 'login_controller@login']);
    Route::post('/handleLogin', ['as' => 'handleLogin', 'uses' => 'login_controller@handle_login']); 
    Route::get('/logout', ['middleware' => 'auth', 'as' => 'logout', 'uses' => 'login_controller@logout']);

    //bidan
    Route::get('/mainpage', ['middleware' => 'auth', 'as' => 'main_page', 'uses' => 'bidan_controller@bidan_home']);
    Route::get('/bidan/editProfile/{id}', ['middleware' => 'auth', 'as' => 'edit_profile', 'uses' => 'bidan_controller@edit_profile']);
    Route::post('/bidan/handleEditProfile', ['middleware' => 'auth', 'as' => 'handle_edit_profile', 'uses' => 'bidan_controller@handle_edit_profile']);

    //hasil tumbuh    
    Route::get('/tumbuh/pilihOrangTua', ['middleware' => 'auth', 'as' => 'choose_orang_tua', 'uses' => 'Hasil_Tumbuh_Controller@choose_orang_tua']);
    Route::get('/tumbuh/pilihAnak/{id}', ['middleware' => 'auth', 'as' => 'choose_anak', 'uses' => 'Hasil_Tumbuh_Controller@choose_anak']);
    Route::get('/tumbuh/lihatTbU/{id}', ['middleware' => 'auth', 'as' => 'view_hasil_tb_u', 'uses' => 'Hasil_Tumbuh_Controller@view_hasil_tb_u']);
    Route::get('/tumbuh/lihatBbU/{id}', ['middleware' => 'auth', 'as' => 'view_hasil_bb_u', 'uses' => 'Hasil_Tumbuh_Controller@view_hasil_bb_u']);
    Route::get('/tumbuh/lihatTbBb/{id}', ['middleware' => 'auth', 'as' => 'view_hasil_tb_bb', 'uses' => 'Hasil_Tumbuh_Controller@view_hasil_tb_bb']);
    Route::get('/tumbuh/pilihTahapTumbuh/{id}', ['middleware' => 'auth', 'as' => 'choose_tahap', 'uses' => 'Hasil_Tumbuh_Controller@choose_tahap']);
    Route::post('/tumbuh/input', ['middleware' => 'auth', 'as' => 'input_tumbuh', 'uses' => 'Hasil_Tumbuh_Controller@input_tumbuh']);
    Route::post('/tumbuh/edit', ['middleware' => 'auth', 'as' => 'edit_tumbuh', 'uses' => 'Hasil_Tumbuh_Controller@edit_tumbuh']);
    Route::post('/tumbuh/delete', ['middleware' => 'auth', 'as' => 'delete_tumbuh', 'uses' => 'Hasil_Tumbuh_Controller@delete_tumbuh']);
});

Route::group(['prefix' => 'api'], function () {
    Route::post('login', 'Login_Api_Controller@login');

    Route::group(['middleware' => ['jwt.auth']], function () {
        Route::post('logout', 'Login_Api_Controller@logout');

        Route::group(['prefix' => 'bidan'], function () {
            Route::get('profile', 'Bidan_Api_Controller@profile');
            Route::post('editProfile', 'Bidan_Api_Controller@edit_profile');
        });
        
        Route::group(['prefix' => 'tumbuh'], function () {
            Route::get('pilihOrangTua', 'Hasil_Tumbuh_Api_Controller@choose_orang_tua');
            Route::get('pilihAnak/{id}', 'Hasil_Tumbuh_Api_Controller@choose_anak');
            Route::get('lihatTbU/{id}', 'Hasil_Tumbuh_Api_Controller@view_hasil_tb_u');
            Route::get('lihatBbU/{id}', 'Hasil_Tumbuh_Api_Controller@view_hasil_bb_u');
            Route::get('lihatTbBb/{id}', 'Hasil_Tumbuh_Api_Controller@view_hasil_tb_bb');
            Route::get('pilihTahapTumbuh/{id}', 'Hasil_Tumbuh_Api_Controller@choose_tahap');
            Route::post('input', 'Hasil_Tumbuh_Api_Controller@input_tumbuh');
            Route::post('edit', 'Hasil_Tumbuh_Api_Controller@edit_tumbuh');
            Route::post('delete', 'Hasil_Tumbuh_Api_Controller@delete_tumbuh');
            // Route::post('editProfile', 'Bidan_Api_Controller@edit_profile');
        });
    });
});
