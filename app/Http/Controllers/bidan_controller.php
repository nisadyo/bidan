<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

class bidan_controller extends Controller
{
    public function bidan_home()
    {
        return view('bidan.bidan_mainpage');
    }

    public function edit_profile($id){
        $bidan = \App\Bidan::find($id);
        $temp = explode('-', $bidan->birthdate);
        $bidan->birthdate = $temp[1] . '/' . $temp[2] . '/' . $temp[0];

        return view('bidan.edit_profile', compact('bidan'));
    }

    public function handle_edit_profile(Request $request){      
        $bidan = \App\Bidan::find($request->id); 
        if($request->password == $request->confirm_password){
            $data = $request->only('nama', 'agama', 'alamat', 'nomor_induk_bidan', 'lokasi_kerja');    

            $temp = strtotime($request->birthdate);
            $data['birthdate'] = date('Y-m-d', $temp);
            $data['password'] = bcrypt($request->password);
            
            $bidan->update($data);
            $edit = true;

            return view('bidan.edit_profile', compact('bidan', 'edit'));
        }else{
            // dd($request->password);
            $not_match = true;

            return view('bidan.edit_profile', compact('bidan', 'not_match'));
        }
    }

    // public function register()
    // {
    //     return view('login.registerView');
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
