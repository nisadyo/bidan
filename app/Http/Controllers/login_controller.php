<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

class login_controller extends Controller
{
    public function login()
    {
        // dd(bcrypt('caca'));
        return view('login.login_view');
    }

    public function handle_login(Request $request)
    {
        $data = $request->only('username','password');

        if(\Auth::attempt($data)){
            session_start();
            return redirect()->intended('mainpage');      
        }

        $error = true;
        
        return view('login.login_view', compact('error'));
    }

    public function logout(){
        \Auth::logout();
        \Session::flush();
        return redirect()->route('login');
    }

    // public function register()
    // {
    //     return view('login.registerView');
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
