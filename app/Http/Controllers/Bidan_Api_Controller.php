<?php

namespace App\Http\Controllers;

use Validator;
use App\Http\Requests\RegisterRequest;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Bidan_Api_Controller extends Controller
{
    public function profile(){
        $data =  JWTAuth::parseToken()->toUser();
        $temp = explode('-', $data->birthdate);
        $data->birthdate = $temp[1] . '/' . $temp[2] . '/' . $temp[0];

        return response()->json([
            "user data" => $data,
        ], 200);
    }

    public function edit_profile(Request $request){
    	$data =  $request->only(['nama', 'agama', 'alamat', 'nomor_induk_bidan', 'lokasi_kerja', 'birthdate', 'password', 'confirm_password']);

        $rules = [
            'nama' => 'required|max:255',
            'agama' => 'required',
            'alamat' => 'required|max:255',
            'birthdate' => 'required',	            
            'password' => 'required',
            'confirm_password' => 'required',
            'nomor_induk_bidan' => 'required|max:255',
            'lokasi_kerja' => 'required|max:255',
        ];

        $message = [
            'nama.required' => 'Anda harus mengisi kolom Nama',
            'agama.required' => 'Anda harus mengisi kolom Agama',
            'alamat.required' => 'Anda harus mengisi kolom Alamat',
            'birthdate.required' => 'Anda harus mengisi kolom Tanggal lahir',
            'password.required' => 'Anda harus mengisi kolom Password',
            'confirm_password.required' => 'Anda harus mengisi kolom Confirm Password',
            'nomor_induk_bidan.required' => 'Anda harus mengisi kolom Nomor Induk Bidan',
            'lokasi_kerja.required' => 'Anda harus mengisi kolom Lokasi Kerja',
        ];

        $validator = Validator::make($data, $rules, $message);

        if ($validator->fails()) {
            return response()->json([
                "response" => "data registrasi tidak valid",
                "reason" => $validator->errors()->all(),
            ],400);
        }
        
    	if($request->password == $request->confirm_password){
    		$id = JWTAuth::parseToken()->toUser()->id;

	        $bidan = \App\Bidan::find($id);

	        $temp = strtotime($request->birthdate);
            $data['birthdate'] = date('Y-m-d', $temp);
            $data['password'] = bcrypt($request->password);
            $data['confirm_password'] = bcrypt($request->confirm_password);
	            
            $bidan->update($data);

	        return response()->json([
	            "response" => "profile berhasil diubah",
	        ], 200);
    	}else{
    		return response()->json(['error' => 'password tidak sesuai'], 400);
    	}
    	
    }
}
