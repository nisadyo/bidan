<?php

namespace App\Http\Controllers;

use Validator;
use App\Http\Requests\RegisterRequest;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Hasil_Tumbuh_Api_Controller extends Controller
{
    public function choose_orang_tua(){
    	$orang_tua = \App\Orangtua::all();
    	
    	return response()->json([
            "orang tua" => $orang_tua,
        ], 200);
    }

    public function choose_anak($id){   	
    	$anak = \App\Anak::where('orangtua_id', $id)->get();

    	return response()->json([
            "orang tua" => $anak,
        ], 200);
    }

    public function view_hasil_tb_u($id){
    	$hasil_tb_u = \App\Hasil_Tumbuh::where('anak_id', $id)->where('parameter__tumbuh_id', '>', 30)->get();
        $anak = \App\Anak::find($id);
        $hasil_tb_u->anak = $anak->nama;
        $hasil_tb_u->orangtua = $anak->orangtua->id;
        foreach ($hasil_tb_u as $h) {
            $temp = \App\Parameter_Tumbuh::find($h->parameter__tumbuh_id);
            $h->usia = $temp->id - 31;
            $standar = \App\Standar_TB_U::where('umur', $h->usia)->first();
            $h->detail_tumbuh = $temp->detail_tumbuh;       
            $h->sangat_pendek = $standar->sangat_pendek;
            $h->pendek = $standar->pendek;
            $h->normal = $standar->normal;

            if($h->tinggi < $h->sangat_pendek){
                $h->hasil = 'sangat pendek';
            }else if($h->tinggi < $h->pendek){
                $h->hasil = 'pendek';
            }else if($h->tinggi <= $h->normal){
                $h->hasil = 'normal';
            }else{
                $h->hasil = 'tinggi';
            }
        }

        return response()->json([
            "hasil tb/u" => $hasil_tb_u,
        ], 200);
    }

    public function view_hasil_bb_u($id){        
        $hasil_bb_u = \App\Hasil_Tumbuh::where('anak_id', $id)->where('parameter__tumbuh_id', '>', 30)->get();
        $anak = \App\Anak::find($id);
        $hasil_bb_u->anak = $anak->nama;
        $hasil_bb_u->orangtua = $anak->orangtua->id;
        foreach ($hasil_bb_u as $h) {
            $temp = \App\Parameter_Tumbuh::find($h->parameter__tumbuh_id);
            $h->usia = $temp->id - 31;
            $standar = \App\Standar_BB_U::where('umur', $h->usia)->first();
            $h->detail_tumbuh = $temp->detail_tumbuh;       
            $h->buruk = $standar->buruk;
            $h->kurang = $standar->kurang;
            $h->baik = $standar->baik;

            if($h->berat < $h->buruk){
                $h->hasil = 'buruk';
            }else if($h->berat < $h->kurang){
                $h->hasil = 'kurang';
            }else if($h->berat <= $h->baik){
                $h->hasil = 'baik';
            }else{
                $h->hasil = 'lebih';
            }
        }

        return response()->json([
            "hasil bb/u" => $hasil_bb_u,
        ], 200);
    }

    public function view_hasil_tb_bb($id){    
        $hasil_tb_bb = \App\Hasil_Tumbuh::where('anak_id', $id)->where('parameter__tumbuh_id', '>', 30)->get();
        $anak = \App\Anak::find($id);
        $hasil_tb_bb->anak = $anak->nama;
        $hasil_tb_bb->orangtua = $anak->orangtua->id;
        foreach ($hasil_tb_bb as $h) {
            $temp = \App\Parameter_Tumbuh::find($h->parameter__tumbuh_id);
            $h->usia = $temp->id - 31;    

            $tinggi = explode('.', $h->tinggi);            
            $tinggi[0] = intval($tinggi[0]);
            $tinggi[1] = intval($tinggi[1]);
            if($tinggi[1] < 10){
                $tinggi[1] = $tinggi[1] * 10;
            }
            if($tinggi[1] > 50){
                $tinggi[1] -= 50;
                $temp2 = 0.5;
            }else{
                $temp2 = 0;
            }

            if($tinggi[1] > 25 && $tinggi[1] <= 50){
                $tinggi = $tinggi[0] + 0.5;
            }else if($tinggi[1] < 25){                
                $tinggi = $tinggi[0] + $temp2;
            }else{                               
                $tinggi = $tinggi[0] + 1;
            }
           
            if($h->usia <= 24){                
                $s = \App\Standar_Tumbuh::where('umur', 24)->where('tinggi_badan', $tinggi)->where('jenis_kelamin', $anak->gender)->first();
            }else{     
                $s = \App\Standar_Tumbuh::where('umur', 60)->where('tinggi_badan', $tinggi)->where('jenis_kelamin', $anak->gender)->first();
            }

            $h->detail_tumbuh = $temp->detail_tumbuh;    
            $h->sangat_kurus = $s['sangat_kurus'];  
            $h->kurus = $s['kurus'];
            $h->normal = $s['normal'];
            $h->gemuk = $s['gemuk'];
            $h->sangat_gemuk = $s['sangat_gemuk'];

            if($h->berat < $h->sangat_kurus){
                $h->hasil = 'sangat kurus';
                $h->gizi = \App\Saran_Gizi::where('status_gizi', $h->hasil)->get();
            }else if($h->berat < $h->kurus){
                $h->hasil = 'kurus';
                $h->gizi = \App\Saran_Gizi::where('status_gizi', $h->hasil)->get();
            }else if($h->berat < $h->normal){
                $h->hasil = 'normal';
                $h->umur = 0;
                if($h->usia > 24){
                    $h->umur = 60;
                }else if($h->usia > 11){
                    $h->umur = 24;
                }else if($h->usia > 9){
                    $h->umur = 11;
                }else if($h->usia > 6){
                    $h->umur = 9;
                }else if($h->usia > 0){
                    $h->umur = 6;
                }
                $h->gizi = \App\Saran_Gizi::where('status_gizi', $h->hasil)->get();
            }else if($h->berat <= $h->gemuk){
                $h->hasil = 'gemuk';
                $h->gizi = \App\Saran_Gizi::where('status_gizi', $h->hasil)->get();
            }else{
                $h->hasil = 'sangat gemuk';
                $h->gizi = \App\Saran_Gizi::where('status_gizi', $h->hasil)->get();
            }
        }

        return response()->json([
            "hasil tb/bb" => $hasil_tb_bb,
        ], 200);
    }

     public function choose($id, $action, $tahap){
        $parameter_tumbuh = \App\Parameter_Tumbuh::all();
        $parameter_tumbuh->id_anak = $id;
        $parameter_tumbuh->user = \Auth::user()->id;
        $parameter_tumbuh->orang_tua = \App\Anak::find($id)->orangtua->id;
        foreach ($parameter_tumbuh as $p) {
            $p->berat = '-';
            $p->tinggi = '-';
            $p->bidan = '-';
            $p->tanggal_input = '-';

            $hasil_tumbuh = \App\Hasil_Tumbuh::where('anak_id', $id)->where('parameter__tumbuh_id', $p->id)->first();
            if($hasil_tumbuh){
                $p->id_hasil_tumbuh = $hasil_tumbuh->id;
                $p->berat = $hasil_tumbuh->berat;
                $p->tinggi = $hasil_tumbuh->tinggi;
                $p->bidan = $hasil_tumbuh->bidan->nama;
                $p->bidan_id = $hasil_tumbuh->bidan->id;
                $temp = explode('-', $hasil_tumbuh->tanggal);
                $p->tanggal_input = $temp[1] . '/' . $temp[2] . '/' . $temp[0];
            }
        }

        return response()->json([
            "parameter tumbuh" => $parameter_tumbuh,
            "action" => $action,
            "tahap" => $tahap,
        ], 200);
    }

    public function choose_tahap($id){
        $action = 'null';
        $tahap = 'null';

    	return $this->choose($id, $action, $tahap);
    }

    public function input_tumbuh(Request $request){
    	$data = $request->only(['parameter__tumbuh_id', 'anak_id', 'bidan_id', 'tanggal', 'berat', 'tinggi']);

        $rules = [
            'tanggal' => 'required',	            
            'berat' => 'required',
            'tinggi' => 'required',
        ];

        $message = [
            'tanggal.required' => 'Anda harus mengisi kolom Tanggal',
            'berat.required' => 'Anda harus mengisi kolom Berat',
            'tinggi.required' => 'Anda harus mengisi kolom Tinggi',
        ];

        $validator = Validator::make($data, $rules, $message);

        if ($validator->fails()) {
            return response()->json([
                "response" => "data tidak valid",
                "reason" => $validator->errors()->all(),
            ],400);
        }

        $data['parameter__tumbuh_id'] = intval($request->parameter__tumbuh_id);
        $data['anak_id'] = intval($request->anak_id);
        $data['bidan_id'] = intval($request->bidan_id);
        $temp = strtotime($request->tanggal);
        $data['tanggal'] = date('Y-m-d', $temp);
        $data['berat'] = floatval($request->berat);
        $data['tinggi'] = floatval($request->tinggi);

        $hasil_tumbuh =  \App\Hasil_Tumbuh::create($data);

        $id = $hasil_tumbuh->anak->id;
        $tahap = \App\Parameter_Tumbuh::find($request->parameter__tumbuh_id)->detail_tumbuh;
        $action = 'add';

        return $this->choose($id, $action, $tahap);
    }

    public function edit_tumbuh(Request $request){
    	$data = $request->only(['id', 'berat', 'tinggi']);

        $rules = [	            
            'berat' => 'required',
            'tinggi' => 'required',
        ];

        $message = [
            'berat.required' => 'Anda harus mengisi kolom Berat',
            'tinggi.required' => 'Anda harus mengisi kolom Tinggi',
        ];

        $validator = Validator::make($data, $rules, $message);

        if ($validator->fails()) {
            return response()->json([
                "response" => "data tidak valid",
                "reason" => $validator->errors()->all(),
            ],400);
        }

        $hasil_tumbuh = \App\Hasil_Tumbuh::find($request->id);
        $data['berat'] = floatval($request->berat);
        $data['tinggi'] = floatval($request->tinggi);
        $hasil_tumbuh->update($data);

        $id = $hasil_tumbuh->anak->id;
        $tahap = \App\Parameter_Tumbuh::find($hasil_tumbuh->parameter__tumbuh_id)->detail_tumbuh;
        $action = 'update';

        return $this->choose($id, $action, $tahap);
    }

    public function delete_tumbuh(Request $request){   
        $hasil_tumbuh = \App\Hasil_Tumbuh::find($request->id);  

        $id = $hasil_tumbuh->anak->id;
        $tahap = \App\Parameter_Tumbuh::find($hasil_tumbuh->parameter__tumbuh_id)->detail_tumbuh;
        $action = 'delete';

        $hasil_tumbuh->delete();

        return $this->choose($id, $action, $tahap);
    }
}
