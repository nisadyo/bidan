<?php

namespace App\Http\Controllers;

use Validator;
use App\Http\Requests\RegisterRequest;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Login_Api_Controller extends Controller
{
    public function login(Request $request){    	
        $data = $request->only('username', 'password');

        try {
            if (!$token = JWTAuth::attempt($data)) {
                return response()->json(['error' => 'data tidak valid'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'token gagal dibuat'], 500);
        }

        return response()->json(compact('token'));
    }

    public function logout(Request $request) {
        $this->validate($request, [
            'token' => 'required'
        ]);

        JWTAuth::invalidate($request->input('token'));
    }
}
