<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Parameter_Tumbuh extends Model
{
    use SoftDeletes;

    protected $table = 'parametertumbuh';

    protected $fillable = [
        'detail_tumbuh', 
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at', 'id',
    ];

    protected $dates =[
      'deleted_at',
    ];

    public function hasil_tumbuh(){
        return $this->hasMany('App\Hasil_Tumbuh');
    }
}
