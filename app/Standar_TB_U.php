<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Standar_TB_U extends Model
{
    use SoftDeletes;

    protected $table = 'standartbu';

    protected $fillable = [
        'umur', 'sangat_pendek', 'pendek', 'normal',
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at', 'id',
    ];

    protected $dates =[
      'deleted_at',
    ];
}
