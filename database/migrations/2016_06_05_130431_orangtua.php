<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Orangtua extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orangtua', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ibu_nama');
            $table->string('ayah_nama');
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->date('birthdate');
            $table->string('agama');
            $table->string('alamat');
            $table->string('kota');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orangtua');
    }
}
