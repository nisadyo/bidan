<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StandarTumbuh extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('standartumbuh', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('umur');
            $table->string('jenis_kelamin', 1);
            $table->double('tinggi_badan', 5, 2);
            $table->double('sangat_kurus', 5, 2);
            $table->double('kurus', 5, 2);
            $table->double('normal', 5, 2);   
            $table->double('gemuk', 5, 2);
            $table->double('sangat_gemuk', 5, 2);         

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('standartumbuh');
    }
}
