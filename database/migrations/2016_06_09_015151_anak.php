<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Anak extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anak', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('orangtua_id')->unsigned();
            $table->string('nama');
            $table->string('gender');
            $table->date('birthdate');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('orangtua_id')
                ->references('id')
                ->on('orangtua')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('anak');
    }
}