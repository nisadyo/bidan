<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StandarBbU extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('standarbbu', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('umur');
            $table->double('buruk', 5, 2);
            $table->double('kurang', 5, 2);
            $table->double('baik', 5, 2);            

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('standarbbu');
    }
}
