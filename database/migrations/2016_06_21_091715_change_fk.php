<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('saran_gizis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('umur')->nullable();
            $table->string('status_gizi');
            $table->longText('saran');   
            $table->longText('keterangan')->nullable();      

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('saran_gizis');
    }
}
