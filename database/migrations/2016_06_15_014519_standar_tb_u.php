<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StandarTbU extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('standartbu', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('umur');
            $table->double('sangat_pendek', 5, 2);
            $table->double('pendek', 5, 2);
            $table->double('normal', 5, 2);            

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('standartbu');
    }
}
