<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HasilTumbuh extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hasiltumbuh', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parameter__tumbuh_id')->unsigned();
            $table->integer('anak_id')->unsigned();    
            $table->integer('bidan_id')->unsigned();      
            $table->date('tanggal');
            $table->double('berat', 5, 2);   
            $table->double('tinggi', 5, 2);          

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('parameter__tumbuh_id')->references('id')->on('parametertumbuh')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('anak_id')->references('id')->on('anak')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('bidan_id')->references('id')->on('bidan')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hasiltumbuh');
    }
}
